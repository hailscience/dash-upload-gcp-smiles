# https://cloud.google.com/storage/docs/uploading-objects#permissions-client-libraries
"""
This script has been modified from there to work with dash uploader...
which requires a lovingly made temporary directory.
"""

import os
from google.cloud import storage


# source_file_name, destination_blob_name):
def upload_blobs(bucket_name, uploadedFiles):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    # blob = bucket.blob(destination_blob_name)
    # def callback_on_completion(status: du.UploadStatus):
    # Create a GCS client and specify the bucket name
    # client = storage.Client()
    # bucket = client.bucket('my-bucket-name')

    # Iterate over the uploaded files
    for uploaded_file in uploadedFiles:
        # Get the file contents and filename
        # contents = uploaded_file["contents"]
        # NOTE: I think with the id in dash-uploader, this will be
        # session-id/filename

        # temp_file_path = "/tmp/" + filename # not necessary with dash uploader, is buffer'd

        # Create a Blob object with the specified filename in the bucket
        # filename = uploaded_file["filename"]

        # use the path we got from the session id lol
        blob = bucket.blob(uploaded_file)

    #     # Upload the file contents to the Blob
    #     blob.upload_from_string(contents)

    # # Return a message indicating that the files were uploaded
    # return html.Div("Files uploaded to GCS!")

    # # Save the file to a temporary file location
    # file = source_file_name # hopefully
    # temp_file_path = "/tmp/" + file.filename
    # file.save(temp_file_path)

        # Upload the file to GCS
        generation_match_precondition = 0
        blob.upload_from_filename(uploaded_file,
                                  if_generation_match=generation_match_precondition)

    # Delete the temporary file
    # os.remove(temp_file_path)

        # print(f"File {file.filename} uploaded to {destination_blob_name}.")

    # Optional: set a generation-match precondition to avoid potential race conditions
    # and data corruptions. The request to upload is aborted if the object's
    # generation number does not match your precondition. For a destination
    # object that does not yet exist, set the if_generation_match precondition to 0.
    # If the destination object already exists in your bucket, set instead a
    # generation-match precondition using its generation number.
    # generation_match_precondition = 0

    # blob.upload_from_filename(
        # source_file_name, if_generation_match=generation_match_precondition)

    # print(
        # f"File {source_file_name} uploaded to {destination_blob_name}."
    # )

# The ID of your GCS bucket
    # bucket_name = os.getenv('yeastgen-fasta')
    # YOU'LL NEVER FIND HER
    # bucket_name = 'yeastgen-fasta'
    # The path to your file to upload
    # source_file_name = "local/path/to/file"
    # The ID of your GCS object
    # destination_blob_name = "storage-object-name"
    # conducting dev in compute engine and deploying to GKE
    # so therefore credentials should all be automatic haha!
