# dash-upload-gcp-smiles

The below is the default readme for gitlab in case I need it later. This is the repo containing my lovely dash-uploader app that takes files and uploads to a google cloud bucket. Hopefully also extensible for GCP SQL integration; no keys are exposed and everything is authenticated using IAM roles in my GCP project.