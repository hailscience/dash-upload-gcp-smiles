################################
# Usage instructions

# to build:
# note the dash-uploader-gcp-smiles-ct is the name of the container,
# this can be customized...
# docker build -t dash-uploader-gcp-smiles-ct .

# to run:
# docker run -p 8050:8050 dash-uploader-gcp-smiles-ct
################################


# Use the official Python base image
FROM python:3.9-slim

# Set the working directory
WORKDIR /app

# Copy the project files into the container
COPY . /app

# Install curl, in case not on in container; this is very much paranaoia lol
RUN apt-get update && apt-get install -y curl

# Install Poetry and confirm installation
RUN curl -sSL https://install.python-poetry.org | python3 -

# Add Poetry to PATH
ENV PATH="/root/.local/bin:${PATH}"

# Verify installation of Poetry
RUN poetry --version

# Install project dependencies with Poetry; these in .toml, no need for pip reqs
RUN poetry install --no-root

# Expose the port that the Dash app will run on
EXPOSE 8080

# DEV: Start the Dash app, basically flask run
# CMD ["poetry", "run", "python", "app/app.py"]

# PROD: Start the Dash app, via gunicorn; we're using poetry to manage everything
CMD ["poetry", "run", "gunicorn", "app.app:server", "-b", "0.0.0.0:8080"]